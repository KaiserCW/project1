<?php

function prepareTitle($title) {
    $title = strtoupper($title);
    $separator = '';
    for ($i = 1; $i <= strlen($title); $i++) {
        $separator .= '*';
    }

    return $title . PHP_EOL . $separator . PHP_EOL;
}

function prepareContent($content) {

//    Declare variable that will contain string with content
    $data = '';

    foreach ($content as $line) {

        $label = isset($line['label']) ? prepareLabel($line['label']) : '';

        // let's check value of key 'type'
        switch ($line['type']) {
            // in case it's numeric array
            case 'item':
                $data .= $label . implode(', ', $line['data']) . PHP_EOL;
                break;
            // in case it's array with named keys
            case 'list':
                unset($line['type']); // because we don't want to print this key's value
                foreach ($line as $key => $value) {
                    $list = is_array($value) ? implode(', ', $value) : $value;
                    $data .= prepareLabel(ucfirst($key)) . $list . PHP_EOL;
                }
                $data .= PHP_EOL;
                break;
            // in case it's simple string
            default:
                $data .= $label . $line['data'] . PHP_EOL;
        }
    }

    return trim($data) . PHP_EOL . PHP_EOL;

}

function prepareLabel($label) {
    $label .= ': ';
    return $label;
}