<?php

require_once __DIR__ . "/data.php";
require_once __DIR__ . "/controller.php";

foreach ($resumeData as $data) {

//    each block’s title must be in uppercase
    $title = prepareTitle($data['title']);

    $content = prepareContent($data['data']);

    $$data['destination'] = $title . $content;

}

require_once __DIR__ . "/template.php";